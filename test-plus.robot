*** Settings ***
Library           RequestsLibrary


*** Test Cases ***


Test x is 1

    ${resp}=     GET    http://127.0.0.1:5000/iseven/1

    # Verify the status code is 200 (OK)
    Should be equal               ${resp.status_code}    ${200}

    # Verify the response of plus operation
    Should Be Equal    ${resp.text}          False


Test x is 0

    ${resp}=     GET    http://127.0.0.1:5000/iseven/0

    # Verify the status code is 200 (OK)
    Should be equal               ${resp.status_code}    ${200}

    # Verify the response of plus operation
    Should Be Equal    ${resp.text}          True


Test x is neg2

    ${resp}=     GET    http://127.0.0.1:5000/iseven/-2

    # Verify the status code is 200 (OK)
    Should be equal               ${resp.status_code}    ${200}

    # Verify the response of plus operation
    Should Be Equal    ${resp.text}          True
